const UserBiodata = require("../models").user_game_biodatas;
const User = require("../models").user_game;
const biodataController = require("../controllers/biodata");
const { validationResult } = require("express-validator");

const mockRequest = ({ body, params, user }) => ({
  body,
  params,
  user,
});

const mockResponse = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

UserBiodata.findAll = jest.fn().mockImplementation(() => [
  { id: 1, firstName: "budi", user_id: "1" },
  { id: 2, firstName: "tono", user_id: "2" },
]);

UserBiodata.findOne = jest
  .fn()
  .mockImplementation(() => ({ id: 1, firstName: "budi", user_id: "1" }));

UserBiodata.findByPk = jest
  .fn()
  .mockImplementation(() => ({ id: 1, firstName: "budi", user_id: "1" }));

UserBiodata.create = jest
  .fn()
  .mockImplementation(() => ({ id: 1, firstName: "budi", user_id: "1" }));

UserBiodata.update = jest
  .fn()
  .mockImplementation(() => [1, [{ id: 1, firstName: "budi", user_id: "1" }]]);

UserBiodata.destroy = jest.fn().mockImplementation(() => {});

jest.mock("express-validator");

validationResult.mockImplementation(() => ({
  isEmpty: () => true,
  array: () => [],
}));

describe("Get Biodata Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({ params: { userId: "1" } });
    const res = mockResponse();

    await biodataController.getBiodata(req, res);

    const biodata = await UserBiodata.findOne();

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      biodata,
    });
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await biodataController.getBiodata(req, res);

    expect(res.status).toBeCalledWith(422);
  });
});

describe("Create Biodata Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      params: { userId: "1" },
      body: {
        first_name: "budi",
        last_name: "setiawan",
        country: "Indonesia",
        email: "budisetiawan@gmail.com",
      },
    });

    const res = mockResponse();

    UserBiodata.findOne.mockImplementationOnce(() => false);

    await biodataController.createBiodata(req, res);

    const biodata = UserBiodata.create();

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      biodata,
    });
  });

  test("409 Already Exists", async () => {
    const req = mockRequest({
      params: { userId: "1" },
      body: {
        first_name: "budi",
        last_name: "setiawan",
        country: "Indonesia",
        email: "budisetiawan@gmail.com",
      },
    });

    const res = mockResponse();

    await biodataController.createBiodata(req, res);

    expect(res.status).toBeCalledWith(409);
  });

  test("404 Not Found", async () => {
    const req = mockRequest({
      params: { userId: "1" },
      body: {
        first_name: "budi",
        last_name: "setiawan",
        country: "Indonesia",
        email: "budisetiawan@gmail.com",
      },
    });

    const res = mockResponse();

    UserBiodata.findOne.mockImplementationOnce(() => false);
    User.findByPk = jest.fn().mockImplementationOnce(() => false);

    await biodataController.createBiodata(req, res);

    expect(res.status).toBeCalledWith(404);
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await biodataController.createBiodata(req, res);

    expect(res.status).toBeCalledWith(422);
  });
});

describe("Update Biodata Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      user: { id: "1" },
      params: { userId: "1" },
      body: {
        first_name: "budi",
        last_name: "setiawan",
        country: "Indonesia",
        email: "budisetiawan@gmail.com",
      },
    });
    const res = mockResponse();

    await biodataController.updateBiodata(req, res);

    const beforeUpdate = UserBiodata.findOne();
    const afterUpdate = UserBiodata.update();

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      message: "User biodata successfully updated",
      beforeUpdate,
      afterUpdate: afterUpdate[1][0],
    });
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await biodataController.updateBiodata(req, res);

    expect(res.status).toBeCalledWith(422);
  });

  test("404 Biodata Not Found", async () => {
    const req = mockRequest({
      user: { id: "2" },
      params: { id: "1" },
      body: {
        username: "budi",
        password: "123456",
      },
    });
    const res = mockResponse();

    UserBiodata.findOne.mockImplementationOnce(() => false);

    await biodataController.updateBiodata(req, res);

    expect(res.status).toBeCalledWith(404);
  });
});

describe("Delete Biodata Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      user: { id: "1" },
      params: { id: "1" },
    });
    const res = mockResponse();

    const biodata = await UserBiodata.findOne();

    await biodataController.deleteBiodata(req, res);

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      message: "User biodata successfully deleted",
      deletedBiodata: biodata,
    });
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await biodataController.deleteBiodata(req, res);

    expect(res.status).toBeCalledWith(422);
  });

  test("404 Biodata Not Found", async () => {
    const req = mockRequest({ params: { userId: "1" } });
    const res = mockResponse();

    UserBiodata.findOne.mockImplementationOnce(() => false);

    await biodataController.deleteBiodata(req, res);

    expect(res.status).toBeCalledWith(404);
  });
});
