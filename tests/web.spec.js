const webController = require("../controllers/web");
const UserHistory = require("../models").user_game_histories;
const UserBiodata = require("../models").user_game_biodatas;
const User = require("../models").user_game;
const axios = require("axios").default;
const jwt = require("jsonwebtoken");

const mockRequest = ({ body, params, user }) => ({
  body,
  params,
  user,
  cookies: {
    token: "token",
  },
  session: { destroy: jest.fn().mockImplementation(() => {}) },
  file: {
    originalname: "original",
  },
});

const mockResponse = () => {
  const res = {};
  res.render = jest.fn().mockReturnValue(res);
  res.redirect = jest.fn().mockReturnValue(res);
  res.cookie = jest.fn().mockReturnValue(res);
  res.clearCookie = jest.fn().mockReturnValue(res);
  return res;
};

jest.mock("axios");
jest.mock("jsonwebtoken");

UserHistory.findAll = jest.fn().mockImplementation(() => [
  { id: 1, title: "game1" },
  { id: 2, title: "game2" },
]);

UserHistory.findByPk = jest.fn().mockImplementation(() => ({
  id: 1,
  title: "game1",
  user_id: 1,
}));

User.findAll = jest.fn().mockImplementation(() => [
  { id: 1, username: "budi" },
  { id: 2, username: "tono" },
]);

UserBiodata.findOne = jest.fn().mockImplementation(() => ({
  id: 1,
  firstName: "budi",
  lastName: "setiawan",
}));

describe("Get Home View", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      user: { id: 1, username: "Budi", isAdmin: false },
    });
    const res = mockResponse();

    webController.getHome(req, res);

    const games = await UserHistory.findAll({
      include: [{ model: User }],
    });

    expect(res.render).toBeCalledWith("index", {
      games: games,
      userId: req.user.id,
      username: req.user.username,
      isAdmin: req.user.role_id === 2,
    });
  });
});

describe("Get Users View", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      user: { id: 1, username: "Budi", isAdmin: false },
    });
    const res = mockResponse();

    const users = await User.findAll({
      include: [{ model: UserBiodata }],
    });

    const usersData = {
      data: {
        users: [
          { id: 1, username: "budi" },
          { id: 2, username: "tono" },
        ],
      },
    };

    axios.get.mockResolvedValueOnce(usersData);

    await webController.getUsers(req, res);

    expect(res.render).toBeCalledWith("users", {
      users: users,
      userId: req.user.id,
      username: req.user.username,
      isAdmin: req.user.role_id === 2,
    });
  });
});

describe("Get Edit Game", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      params: { id: 1 },
      user: { id: 1, username: "Budi", isAdmin: false },
    });
    const res = mockResponse();

    const game = await UserHistory.findByPk(1, {
      include: [{ model: User }],
    });

    await webController.getEditGame(req, res);

    expect(res.render).toBeCalledWith("edit-game", {
      game: game,
    });
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({
      params: { id: null },
      user: { id: 1, username: "Budi", isAdmin: false },
    });
    const res = mockResponse();

    await webController.getEditGame(req, res);

    expect(res.render).toBeCalledWith("error", {
      data: new Error("Valid Game ID param is required"),
    });
  });

  test("404 Game Not Found", async () => {
    UserHistory.findByPk = jest.fn().mockImplementationOnce(() => null);

    const req = mockRequest({
      params: { id: -1 },
      user: { id: 1, username: "Budi", isAdmin: false },
    });
    const res = mockResponse();

    await webController.getEditGame(req, res);

    expect(res.render).toBeCalledWith("error", {
      data: new Error("Game Not Found"),
    });
  });

  test("401 Unathorized", async () => {
    const req = mockRequest({
      params: { id: 1 },
      user: { id: 7, username: "Budi", isAdmin: false },
    });
    const res = mockResponse();

    UserHistory.findByPk.mockImplementationOnce(() => ({
      user_id: 1,
    }));

    await webController.getEditGame(req, res);

    expect(res.redirect).toBeCalledWith("/web");
  });
});

describe("Post Edit Game", () => {
  test("200 Success", async () => {
    const updatedGameInfo = {
      title: "Updated Game",
      publisher: "Updated Publisher",
      score: 100,
      hours_time: 50,
    };

    const req = mockRequest({ body: updatedGameInfo, params: { id: 1 } });
    const res = mockResponse();

    axios.patch = jest.fn().mockImplementationOnce(() => true);

    await webController.postEditGame(req, res);

    expect(res.redirect).toBeCalledWith("/web");
  });

  test("422 Validation Error", async () => {
    const updatedGameInfo = {
      title: "Updated Game",
      publisher: "Updated Publisher",
      score: 100,
      hours_time: 50,
    };

    const req = mockRequest({ body: updatedGameInfo, params: { id: null } });
    const res = mockResponse();

    await webController.postEditGame(req, res);

    expect(res.render).toBeCalledWith("error", {
      data: new Error("Valid Game ID param is required"),
    });
  });
});

describe("Delete Game", () => {
  test("200 Success", async () => {
    axios.delete = jest.fn().mockImplementationOnce(() => true);

    const req = mockRequest({ params: { id: 1 } });
    const res = mockResponse();

    await webController.deleteGame(req, res);

    expect(res.redirect).toBeCalledWith("/web");
  });

  test("500 System Error", async () => {
    const req = mockRequest({ params: { id: 1 } });
    const res = mockResponse();

    axios.delete.mockImplementationOnce(() => Promise.reject("error"));

    await webController.deleteGame(req, res);

    expect(res.render).toBeCalledTimes(1);
  });
});

describe("Get Add Game", () => {
  test("200 Success", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    webController.getAddGame(req, res);

    expect(res.render).toBeCalledWith("add-game");
  });
});

describe("Post Add Game", () => {
  test("200 Success", async () => {
    axios.post = jest.fn().mockImplementationOnce(() => true);

    const newGameInfo = {
      username: "budi",
      title: "New Game",
      publisher: "New Publisher",
      score: 100,
      hours_time: 50,
    };

    const req = mockRequest({ body: newGameInfo, user: { id: 1 } });
    const res = mockResponse();

    await webController.postAddGame(req, res);

    expect(res.redirect).toBeCalledWith("/web");
  });

  test("404 User Not Found", async () => {
    User.findOne = jest.fn().mockImplementationOnce(() => null);

    const newGameInfo = {
      username: "unknown0",
      title: "New Game",
      publisher: "New Publisher",
      score: 100,
      hours_time: 50,
    };

    const req = mockRequest({ body: newGameInfo, user: { id: 1 } });
    const res = mockResponse();

    await webController.postAddGame(req, res);

    expect(res.render).toBeCalledWith("error", {
      data: new Error("User Not Found"),
    });
  });
});

describe("Get Register View", () => {
  test("200 Success", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    webController.getRegister(req, res);

    expect(res.render).toBeCalledWith("register");
  });
});

describe("Post Register", () => {
  test("200 Success", async () => {
    const newUserInfo = {
      username: "NewUser",
      password: "123456",
      firstName: "FirstName",
      lastName: "LastName",
      country: "Country",
      email: "newuser@gmail.com",
      isAdmin: false,
    };

    const req = mockRequest({ body: newUserInfo });
    const res = mockResponse();

    axios.post = jest.fn().mockImplementation(() => ({
      data: {
        user: {
          id: 1,
        },
      },
    }));

    await webController.postRegister(req, res);

    expect(res.redirect).toBeCalledWith("/web");
  });

  test("Error", async () => {
    const newUserInfo = {
      username: "NewUser",
      password: "123456",
      firstName: "FirstName",
      lastName: "LastName",
      country: "Country",
      email: "newuser@gmail.com",
      isAdmin: false,
    };

    const req = mockRequest({ body: newUserInfo });
    const res = mockResponse();

    const error = new Error("Error Text");
    axios.post = jest.fn().mockImplementation(() => {
      throw error;
    });

    await webController.postRegister(req, res);

    expect(res.render).toBeCalledWith("error", {
      data: error,
    });
  });
});

describe("Get Login View", () => {
  test("200 Success", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    webController.getLogin(req, res);

    expect(res.render).toBeCalledWith("login");
  });
});

describe("Post Login", () => {
  test("200 Success", async () => {
    const userInfo = {
      username: "budi",
      password: "123456",
    };

    const req = mockRequest({ body: userInfo });
    const res = mockResponse();

    axios.post = jest.fn().mockImplementationOnce(() => ({
      status: 200,
      data: {
        token: "abc",
      },
    }));

    await webController.postLogin(req, res);

    expect(res.redirect).toBeCalledWith("/web");
  });

  test("401 Wrong Username / Password", async () => {
    const userInfo = {
      username: "budi",
      password: "wrong",
    };

    const req = mockRequest({ body: userInfo });
    const res = mockResponse();

    axios.post = jest.fn().mockImplementationOnce(() => ({
      status: 400,
      data: {
        message: "Wrong Username / Password",
      },
    }));

    await webController.postLogin(req, res);

    expect(res.render).toBeCalledWith("error", {
      data: new Error("Wrong Username / Password"),
    });
  });
});

describe("Get Edit User", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      params: { userId: 1 },
      user: {
        id: 1,
        username: "Budi",
        isAdmin: false,
      },
    });
    const res = mockResponse();

    const biodata = await UserBiodata.findOne({
      where: { user_id: 1 },
      include: [{ model: User }],
    });

    axios.get.mockResolvedValueOnce({
      data: {
        biodata: biodata,
      },
    });

    await webController.getEditUser(req, res);

    expect(res.render).toBeCalledWith("edit-user", {
      userData: biodata,
    });
  });

  test("401 Unauthorized", async () => {
    const req = mockRequest({
      params: { userId: 1 },
      user: {
        id: 2,
        username: "Budi",
        isAdmin: false,
      },
    });
    const res = mockResponse();

    await webController.getEditUser(req, res);

    expect(res.redirect).toBeCalledWith("/web/users");
  });
});

describe("Post Edit User", () => {
  test("200 Success", async () => {
    const updatedUserInfo = {
      firstName: "UpdatedFirstName",
      lastName: "UpdatedLastName",
      country: "UpdatedCountry",
      email: "updatedemail@gmail.com",
    };

    const req = mockRequest({
      body: updatedUserInfo,
      params: { userId: 1 },
      user: {
        id: 1,
        username: "Budi",
        isAdmin: false,
      },
    });
    const res = mockResponse();

    axios.patch = jest.fn().mockImplementation(() => true);

    await webController.postEditUser(req, res);

    expect(res.redirect).toBeCalledWith("/web/users");
  });

  test("401 Unauthorized", async () => {
    const req = mockRequest({
      params: { userId: 2 },
      user: {
        id: 1,
        username: "Budi",
        isAdmin: false,
      },
    });
    const res = mockResponse();

    await webController.postEditUser(req, res);

    expect(res.redirect).toBeCalledWith("/web/users");
  });

  test("500 System Error", async () => {
    const updatedUserInfo = {
      firstName: "UpdatedFirstName",
      lastName: "UpdatedLastName",
      country: "UpdatedCountry",
      email: "updatedemail@gmail.com",
    };

    const req = mockRequest({
      body: updatedUserInfo,
      params: { userId: 1 },
      user: {
        id: 1,
        username: "Budi",
        isAdmin: false,
      },
    });
    const res = mockResponse();

    axios.patch.mockImplementation(() => Promise.reject());

    await webController.postEditUser(req, res);

    expect(res.render).toBeCalledTimes(1);
  });
});

describe("Post Delete User", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      params: { userId: 1 },
      user: {
        id: 1,
        username: "Budi",
        isAdmin: false,
      },
    });
    const res = mockResponse();

    axios.delete = jest.fn().mockImplementationOnce(() => true);

    await webController.postDeleteUserBio(req, res);

    expect(res.redirect).toBeCalledWith("/web/users");
  });

  test("Unauthorized", async () => {
    const req = mockRequest({
      params: { userId: 2 },
      user: {
        id: 1,
        username: "Budi",
        isAdmin: false,
      },
    });
    const res = mockResponse();

    await webController.postDeleteUserBio(req, res);

    expect(res.redirect).toBeCalledWith("/web/users");
  });

  test("500 System Error", async () => {
    const req = mockRequest({
      params: { userId: 1 },
      user: {
        id: 1,
        username: "Budi",
        isAdmin: false,
      },
    });
    const res = mockResponse();

    axios.delete.mockImplementationOnce(() => Promise.reject());

    await webController.postDeleteUserBio(req, res);

    expect(res.render).toBeCalledTimes(1);
  });
});

describe("Get Auth View", () => {
  test("Success", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    jwt.verify.mockImplementationOnce(() => false);

    webController.getAuthView(req, res);

    expect(res.render).toBeCalledWith("auth");
  });

  test("Redirect to Home", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    jwt.verify.mockImplementationOnce(() => true);

    webController.getAuthView(req, res);

    expect(res.redirect).toBeCalledWith("/web");
  });
});

describe("Post Logout User", () => {
  test("Success", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    webController.postLogout(req, res);

    expect(req.session.destroy).toBeCalledTimes(1);
    expect(res.clearCookie).toBeCalledWith("token");
    expect(res.redirect).toBeCalledWith("/web/auth");
  });
});
