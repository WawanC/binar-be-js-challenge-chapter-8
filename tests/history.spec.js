const UserHistory = require("../models").user_game_histories;
const historyController = require("../controllers/history");
const { validationResult } = require("express-validator");

const mockRequest = ({ body, params, user }) => ({
  body,
  params,
  user,
});

const mockResponse = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

UserHistory.findAll = jest.fn().mockImplementation(() => [
  { id: 1, title: "gameA" },
  { id: 2, title: "gameB" },
]);

UserHistory.findByPk = jest
  .fn()
  .mockImplementation(() => ({ id: 1, title: "gameA", user_id: "1" }));

UserHistory.create = jest
  .fn()
  .mockImplementation(() => ({ id: 1, title: "gameA" }));

UserHistory.update = jest
  .fn()
  .mockImplementation(() => [1, [{ id: 1, title: "gameA", user_id: "1" }]]);

UserHistory.destroy = jest.fn().mockImplementation(() => {});

jest.mock("express-validator");

validationResult.mockImplementation(() => ({
  isEmpty: () => true,
  array: () => [],
}));

describe("Get Histories Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    historyController.getHistories(req, res);

    const games = await UserHistory.findAll();

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      games,
    });
  });
});

describe("Get History Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({ params: { userId: "1" } });
    const res = mockResponse();

    await historyController.getHistory(req, res);

    const games = await UserHistory.findAll();

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      games,
    });
  });
  test("422 Validation Error", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await historyController.getHistory(req, res);

    expect(res.status).toBeCalledWith(422);
  });
});

describe("Create History Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      params: { userId: "1" },
      body: {
        title: "gameC",
        publisher: "publisherC",
        score: 100,
        hours_time: 100,
        file: "file",
      },
    });
    const res = mockResponse();

    await historyController.createHistory(req, res);

    const newGame = UserHistory.create();

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      game: newGame,
    });
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await historyController.createHistory(req, res);

    expect(res.status).toBeCalledWith(422);
  });
});

describe("Update History Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      user: { id: "1" },
      params: { gameId: "1" },
      body: {
        title: "gameC",
        publisher: "publisherC",
        score: 100,
        hours_time: 100,
        file: "file",
      },
    });
    const res = mockResponse();

    await historyController.updateHistory(req, res);

    const beforeUpdate = UserHistory.findByPk();
    const afterUpdate = UserHistory.update();

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      message: "User games history successfully updated",
      beforeUpdate,
      afterUpdate: afterUpdate[1][0],
    });
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await historyController.updateHistory(req, res);

    expect(res.status).toBeCalledWith(422);
  });

  test("401 Unauthorized", async () => {
    const req = mockRequest({
      user: { id: "2" },
      params: { id: "1" },
      body: {
        username: "budi",
        password: "123456",
      },
    });
    const res = mockResponse();

    await historyController.updateHistory(req, res);

    expect(res.status).toBeCalledWith(401);
  });

  test("404 History Not Found", async () => {
    const req = mockRequest({
      user: { id: "1" },
      params: { gameId: "1" },
      body: {
        title: "gameC",
        publisher: "publisherC",
        score: 100,
        hours_time: 100,
        file: "file",
      },
    });
    const res = mockResponse();

    UserHistory.findByPk.mockImplementationOnce(() => false);

    await historyController.updateHistory(req, res);

    expect(res.status).toBeCalledWith(404);
    expect(res.json).toBeCalledWith({
      message: "Game history is not found",
    });
  });
});

describe("Delete History Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      user: { id: "1" },
      params: { id: "1" },
    });
    const res = mockResponse();

    const game = await UserHistory.findByPk();

    await historyController.deleteHistory(req, res);

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      message: "User game history successfully deleted",
      beforeDeleted: game,
    });
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await historyController.deleteHistory(req, res);

    expect(res.status).toBeCalledWith(422);
  });

  test("401 Unauthorized", async () => {
    const req = mockRequest({
      user: { id: "2" },
      params: { id: "1" },
    });
    const res = mockResponse();

    await historyController.deleteHistory(req, res);

    expect(res.status).toBeCalledWith(401);
  });

  test("404 Game Not Found", async () => {
    const req = mockRequest({
      user: { id: "1" },
      params: { id: "1" },
    });
    const res = mockResponse();

    UserHistory.findByPk.mockImplementationOnce(() => false);

    await historyController.deleteHistory(req, res);

    expect(res.status).toBeCalledWith(404);
  });
});
