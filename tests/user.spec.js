const User = require("../models").user_game;
const userController = require("../controllers/user");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const mockRequest = ({ body, params, user }) => ({
  body,
  params,
  user,
});

const mockResponse = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

User.findAll = jest.fn().mockImplementation(() => [
  { id: 1, username: "budi" },
  { id: 2, username: "tono" },
]);

User.findByPk = jest
  .fn()
  .mockImplementation(() => ({ id: 1, username: "budi" }));

User.create = jest.fn().mockImplementation(() => ({ username: "New User" }));

User.update = jest
  .fn()
  .mockImplementation(() => [1, [{ id: 1, username: "budi" }]]);

User.destroy = jest.fn().mockImplementation(() => {});

jest.mock("express-validator");
jest.mock("jsonwebtoken");
jest.mock("bcryptjs");

jwt.sign.mockImplementation(() => "token");

validationResult.mockImplementation(() => ({
  isEmpty: () => true,
  array: () => [],
}));

bcrypt.compare.mockImplementation(() => true);

describe("Get Users Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    userController.getUsers(req, res);

    const users = await User.findAll();

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      users,
    });
  });
});

describe("Get User Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({ params: { id: "1" } });
    const res = mockResponse();

    await userController.getUser(req, res);

    const user = await User.findByPk();

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      user,
    });
  });
  test("422 Validation Error", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await userController.getUser(req, res);

    expect(res.status).toBeCalledWith(422);
  });
});

describe("Create User Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      body: {
        username: "budi",
        password: "123456",
      },
    });
    const res = mockResponse();

    await userController.createUser(req, res);

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      user: { username: "New User" },
      token: "token",
    });
  });

  test("200 Success Admin", async () => {
    const req = mockRequest({
      body: {
        username: "budi",
        password: "123456",
        isAdmin: true,
      },
    });
    const res = mockResponse();

    await userController.createUser(req, res);

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      user: { username: "New User" },
      token: "token",
    });
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await userController.createUser(req, res);

    expect(res.status).toBeCalledWith(422);
  });
});

describe("Update User Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      user: { id: "1" },
      params: { id: "1" },
      body: {
        username: "budi",
        password: "123456",
      },
    });
    const res = mockResponse();

    await userController.updateUser(req, res);

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      message: "User successfully updated",
      beforeUpdate: { id: 1, username: "budi" },
      afterUpdate: { id: 1, username: "budi" },
    });
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await userController.updateUser(req, res);

    expect(res.status).toBeCalledWith(422);
  });

  test("401 Unauthorized", async () => {
    const req = mockRequest({
      user: { id: "2" },
      params: { id: "1" },
      body: {
        username: "budi",
        password: "123456",
      },
    });
    const res = mockResponse();

    await userController.updateUser(req, res);

    expect(res.status).toBeCalledWith(401);
  });

  test("404 User Not Found", async () => {
    const req = mockRequest({
      user: { id: "1" },
      params: { id: "1" },
      body: {
        username: "budi",
        password: "123456",
      },
    });
    const res = mockResponse();

    User.findByPk.mockImplementationOnce(() => false);

    await userController.updateUser(req, res);

    expect(res.status).toBeCalledWith(404);
  });
});

describe("Delete User Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      user: { id: "1" },
      params: { id: "1" },
    });
    const res = mockResponse();

    const user = await User.findByPk();

    await userController.deleteUser(req, res);

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      message: "User successfully deleted",
      deletedUser: user,
    });
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await userController.deleteUser(req, res);

    expect(res.status).toBeCalledWith(422);
  });

  test("401 Unauthorized", async () => {
    const req = mockRequest({
      user: { id: "2" },
      params: { id: "1" },
    });
    const res = mockResponse();

    await userController.deleteUser(req, res);

    expect(res.status).toBeCalledWith(401);
  });

  test("404 User Not Found", async () => {
    const req = mockRequest({
      user: { id: "1" },
      params: { id: "1" },
    });
    const res = mockResponse();

    User.findByPk.mockImplementationOnce(() => false);

    await userController.deleteUser(req, res);

    expect(res.status).toBeCalledWith(404);
  });
});

describe("Login User Function", () => {
  test("200 Success", async () => {
    const req = mockRequest({
      body: {
        username: "budi",
        password: "123456",
      },
    });
    const res = mockResponse();

    await userController.login(req, res);

    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith({
      message: "Login Success",
      token: "token",
    });
  });

  test("422 Validation Error", async () => {
    const req = mockRequest({
      body: {
        username: "budi",
        password: "123456",
      },
    });
    const res = mockResponse();

    validationResult.mockImplementationOnce(() => ({
      isEmpty: () => false,
      array: () => [],
    }));

    await userController.login(req, res);

    expect(res.status).toBeCalledWith(422);
  });

  test("403 Wrong Info", async () => {
    const req = mockRequest({
      body: {
        username: "budi",
        password: "123456",
      },
    });
    const res = mockResponse();

    bcrypt.compare.mockImplementationOnce(() => false);

    await userController.login(req, res);

    expect(res.status).toBeCalledWith(403);
    expect(res.json).toBeCalledWith({
      message: "Wrong Username / Password",
    });
  });
});
