const router = require("express").Router();
const { param, body } = require("express-validator");
const userController = require("../controllers/user");
const passport = require("passport");
const jwtStrategyAPI = require("../utils/passport").jwtStrategyAPI;

router.get("/", userController.getUsers);
router.get(
  "/:id",
  param("id").notEmpty().isInt().withMessage("Valid User ID is required"),
  userController.getUser
);
router.post(
  "/",
  body(["username", "password"])
    .notEmpty()
    .withMessage("Username and Password is required"),
  userController.createUser
);
router.patch(
  "/:id",
  passport.authenticate(jwtStrategyAPI, { session: false }),
  param("id").notEmpty().isInt().withMessage("Valid User ID is required"),
  userController.updateUser
);
router.delete(
  "/:id",
  passport.authenticate(jwtStrategyAPI, { session: false }),
  param("id").notEmpty().isInt().withMessage("Valid User ID is required"),
  userController.deleteUser
);

router.post(
  "/login",
  body(["username", "password"])
    .notEmpty()
    .withMessage("Username and Password is required"),
  userController.login
);

module.exports = router;
