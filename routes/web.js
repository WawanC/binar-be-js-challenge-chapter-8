const router = require("express").Router();
const webController = require("../controllers/web");
const passport = require("passport");
const jwtStrategy = require("../utils/passport").jwtStrategy;
const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, path.join(__dirname, "..", "public", "assets"));
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname);
  },
});

router.get(
  "/",
  passport.authenticate(jwtStrategy, { failureRedirect: "/web/auth" }),
  webController.getHome
);
router.get(
  "/users",
  passport.authenticate(jwtStrategy, { failureRedirect: "/web/auth" }),
  webController.getUsers
);

router.post(
  "/delete-game/:id",
  passport.authenticate(jwtStrategy, { failureRedirect: "/web/auth" }),
  webController.deleteGame
);

router.get(
  "/add-game",
  passport.authenticate(jwtStrategy, { failureRedirect: "/web/auth" }),

  webController.getAddGame
);
router.post(
  "/add-game",
  passport.authenticate(jwtStrategy, { failureRedirect: "/web/auth" }),
  multer({ storage: storage }).single("file"),
  webController.postAddGame
);

router.get(
  "/edit-game/:id",
  passport.authenticate(jwtStrategy, { failureRedirect: "/web/auth" }),
  webController.getEditGame
);
router.post(
  "/edit-game/:id",
  passport.authenticate(jwtStrategy, { failureRedirect: "/web/auth" }),
  webController.postEditGame
);

// router.get("/register", webController.getRegister);
router.post("/register", webController.postRegister);

// router.get("/login", webController.getLogin);
router.post("/login", webController.postLogin);

router.get("/auth", webController.getAuthView);
router.post(
  "/logout",
  passport.authenticate(jwtStrategy, { failureRedirect: "/web/auth" }),
  webController.postLogout
);

router.get(
  "/edit-user/:userId",
  passport.authenticate(jwtStrategy, { failureRedirect: "/web/auth" }),
  webController.getEditUser
);
router.post(
  "/edit-user/:userId",
  passport.authenticate(jwtStrategy, { failureRedirect: "/web/auth" }),
  webController.postEditUser
);
router.post(
  "/delete-user/:userId",
  passport.authenticate(jwtStrategy, { failureRedirect: "/web/auth" }),
  webController.postDeleteUserBio
);

module.exports = router;
